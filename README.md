### Component React Native

This application display data from API randomly in 3 second.

How to run ?
1. Clone
    - SSH: git@gitlab.com:RakaJhai/react-native-project.git
    - HTTPS: https://gitlab.com/RakaJhai/react-native-project.git
2. npm install => to install all modules
    - You can delet directory "/node_modules" before run npm install
3. npm start
4. npx react-native run-android or npm run android