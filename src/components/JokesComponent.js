import React from 'react';
import axios from 'axios';
import { View, Text, StyleSheet } from 'react-native';

class JokesComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listJoke:[],
            jokesSelected:[]
        }
    }

    componentDidMount(){
        axios.get('http://localhost:5000/jokes')
        .then(res => {
            // console.log(res.data)
            this.setState({listJoke: res.data})
            const interval = setInterval(() => {
                const randomdata= Math.floor(Math.random() * this.state.listJoke.length)
                this.setState({jokesSelected: this.state.listJoke[randomdata]})
            }, 3000);
        }).catch(err =>{
            console.log(err, "Network Error!!!")
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.jokesText}>{this.state.jokesSelected.jokes}</Text>
            </View>
        )
    }
}

export default JokesComponent;

const styles =StyleSheet.create({
    container:{
        minHeight: 100,
        borderRadius: 20,
        backgroundColor: "orange",
        justifyContent: 'center',
        marginBottom: 20
    },
    jokesText:{
        padding: 20
    }
})