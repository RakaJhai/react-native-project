import axios from 'axios';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class WeatherComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listWeather:[],
            weatherSelected:{}
        }
    }

    componentDidMount(){
        this.getData();
    }

    async getData(){
        try{
            let res = await axios.get('http://localhost:5000/weather')
            this.setState({listWeather: res.data})
            this.interval = setInterval(()=>{
                const randomWeather = Math.floor(Math.random()*this.state.listWeather.length);
                this.setState({weatherSelected: this.state.listWeather[randomWeather]})
            }, 3000)
        }
        catch(err){
            console.log(err, 'ERROR WEATHER!!!')
        }
    }

    render(){
        return(
            <View style={styles.weatherContent}>
                <View>
                    <Text style={styles.location}>{this.state.weatherSelected.location}</Text>
                    <Text style={styles.short_note}>{this.state.weatherSelected.short_note}</Text>
                </View>
                <View style={styles.first_content}>
                    <Text style={styles.suhu}>Suhu: {this.state.weatherSelected.suhu}</Text>
                    <Text style={styles.kelembaban}>Kelembaban: {this.state.weatherSelected.kelembaban}</Text>
                </View>
                <View style={styles.second_content}>    
                    <Text style={styles.angin}>Angin: {this.state.weatherSelected.angin}</Text>
                    <Text style={styles.presipitasi}>Presipitasi: {this.state.weatherSelected.presipitasi}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    weatherContent:{
        borderRadius: 20,
        // borderWidth: 2,
        height: 170,
        backgroundColor: "#00e7f7",
        
    },
    location:{
        fontSize: 36,
        textTransform: "uppercase",
        textAlign: "center",
        fontWeight: '700',
        borderBottomColor: '#ffffff',
        borderBottomWidth: 1,
    },
    short_note:{
        fontSize: 16,
        textAlign: "center"
    },
    first_content:{
        flexDirection: 'row',
        flexWrap: "wrap-reverse",
        justifyContent: 'center',
        padding: 10,
    },
    second_content:{
        flexDirection: 'row',
        flexWrap: "wrap-reverse",
        justifyContent: 'center',
        paddingVertical: 10
    },
    suhu:{
        fontSize: 18,
        fontWeight: '700',
    },
    kelembaban:{
        paddingHorizontal: 20,
        fontSize: 18,
        fontWeight: '700',
    },
    angin:{
        fontSize: 20,
        fontWeight: '700',
    },
    presipitasi:{
        paddingHorizontal: 20,
        fontSize: 20,
        fontWeight: '700',
    }
})