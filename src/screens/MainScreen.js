import React from 'react';
import JokesComponent from '../components/JokesComponent';
import WeatherComponent from '../components/WeatherComponent';
import CorouselComponent from '../components/CorouselComponent';
import { View } from 'react-native';

function MainScreen(){
    return(
        <View>
            <CorouselComponent/>
            <JokesComponent />
            <WeatherComponent />         
        </View>
    )
}

export default MainScreen;